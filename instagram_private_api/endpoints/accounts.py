import json
import random
import time
from collections import OrderedDict
from socket import timeout, error as SocketError
from ssl import SSLError

from ..compat import (
    compat_urllib_request, compat_urllib_error,
    compat_http_client
)
from ..compatpatch import ClientCompatPatch
from ..constants import Constants
from ..errors import (
    ClientError, ClientLoginError, ClientConnectionError,
    ClientThrottledError, ClientLoginRequiredError)
from ..http import MultipartFormDataEncoder

try:
    ConnectionError = ConnectionError  # pylint: disable=redefined-builtin
except NameError:  # Python 2:
    class ConnectionError(Exception):
        pass


class AccountsEndpointsMixin(object):
    """For endpoints in ``/accounts/``."""

    def getBlockedMedia(self):
        return self._call_api('media/blocked/')

    def storeClientPushPermissions(self):
        params = dict(_csrftoken=self.csrftoken,
                      _uuid=self.uuid,
                      device_id=self.device_id,
                      enabled=True)

        self._call_api('notifications/store_client_push_permissions/', params=params, unsigned=True)

    def sendSupportedCapabilities(self):
        params = dict(supported_capabilities_new=json.dumps(Constants.SUPPORTED_CAPABILITIES, separators=(',', ':')),
                      _csrftoken=self.csrftoken,
                      _uuid=self.uuid,
                      _uid=self.authenticated_user_id)

        self._call_api('creatives/write_supported_capabilities/', params=params)

    def getAccountFamily(self):
        return self._call_api('multiple_accounts/get_account_family/')

    def getLinkageStatus(self):
        return self._call_api('linked_accounts/get_linkage_status/')

    def getLoomFetchConfig(self):
        return self._call_api('loom/fetch_config/')

    def getDeviceCapabilitiesDecisions(self):
        json_params = json.dumps({}, separators=(',', ':'))
        hash_sig = self._generate_signature(json_params)
        query = OrderedDict(
            (
                ('signed_body', hash_sig + '.' + json_params),
                ('ig_sig_key_version', self.key_version),
            )
        )

        self._call_api('device_capabilities/decisions/', query=query)

    def getQPCooldowns(self):
        json_params = json.dumps({}, separators=(',', ':'))
        hash_sig = self._generate_signature(json_params)
        query = OrderedDict(
            (
                ('signed_body', hash_sig + '.' + json_params),
                ('ig_sig_key_version', self.key_version),
            )
        )

        self._call_api('qp/get_cooldowns/', query=query)

    def getQPFetch(self):
        params = dict(vc_policy='default',
                      _csrftoken=self.csrftoken,
                      _uid=self.authenticated_user_id,
                      _uuid=self.uuid,
                      surfaces_to_queries=json.dumps(
                          {
                              Constants.BATCH_SURFACES[0][0]: Constants.BATCH_QUERY,
                              Constants.BATCH_SURFACES[1][0]: Constants.BATCH_QUERY,
                              Constants.BATCH_SURFACES[2][0]: Constants.BATCH_QUERY,
                          }
                      ),
                      surfaces_to_triggers=json.dumps(
                          {
                              Constants.BATCH_SURFACES[0][0]: Constants.BATCH_SURFACES[0][1],
                              Constants.BATCH_SURFACES[1][0]: Constants.BATCH_SURFACES[1][1],
                              Constants.BATCH_SURFACES[2][0]: Constants.BATCH_SURFACES[2][1],
                          }
                      ),
                      version=Constants.BATCH_VERSION,
                      scale=Constants.BATCH_SCALE,
                      )

        self._call_api('qp/batch_fetch/', params=params)

    def getBootstrapUsers(self):
        try:
            surfaces = [
                'autocomplete_user_list',
                'coefficient_besties_list_ranking',
                'coefficient_rank_recipient_user_suggestion',
                'coefficient_ios_section_test_bootstrap_ranking',
                'coefficient_direct_recipients_ranking_variant_2',
            ]
            self._call_api('scores/bootstrap/users/', query=dict(surfaces=json.dumps(surfaces, separators=(',', ':'))))
        except ClientThrottledError:
            pass

    def syncUserFeatures(self):
        params = dict(
            id=self.authenticated_user_id,
            experiments=Constants.LOGIN_EXPERIMENTS,
            _csrftoken=self.csrftoken,
            _uuid=self.uuid,
            _uid=self.authenticated_user_id,
        )
        # Тут PHP библиотека сохраняем эксперименты которые ей возвращает сервер, и в дальнейшем эмулирует другие параметры при работе приложения с сервером
        result = self._call_api('qe/sync/', params=params, update_headers={'X-DEVICE-ID': self.uuid})
        self.last_experiments = time.time()
        return result

    def getProcessContactPointSignals(self):
        try:
            params = dict(
                google_tokens='[]',
                phone_id=self.phone_id,
                _uid=self.authenticated_user_id,
                _uuid=self.uuid,
                device_id=self.device_id,
                _csrftoken=self.csrftoken,
            )

            return self._call_api('accounts/process_contact_point_signals/', params=params)
        except ClientThrottledError:
            pass

    def getArlinkDownloadInfo(self):
        query = dict(
            version_override='2.2.1',
        )

        return self._call_api('users/arlink_download_info/', query=query)

    def getSharePrefill(self):
        query = dict(
            views='["story_share_sheet","threads_people_picker","reshare_share_sheet"]',
        )

        return self._call_api('banyan/banyan/', query=query)

    def getPresences(self):
        return self._call_api('direct_v2/get_presence/')
    
    def getFacebookOTA(self):
        query = dict(
            fields=Constants.FACEBOOK_OTA_FIELDS,
            custom_user_id=self.authenticated_user_id,
            signed_body=self._generate_signature('') + '.',
            ig_sig_key_version=Constants.SIG_KEY_VERSION,
            version_code=Constants.VERSION_CODE,
            version_name=Constants.APP_VERSION,
            custom_app_id=Constants.FACEBOOK_ORCA_APPLICATION_ID,
            custom_device_id=self.uuid,
        )

        return self._call_api('facebook_ota/', query=query)

    def getRankedRecipients(self, mode, show_threads, query=None):
        try:
            params = dict(mode=mode,
                          show_threads=show_threads,
                          use_unified_inbox=True
                          )
            return self._call_api('direct_v2/ranked_recipients/', params=params, query=query)
        except ClientThrottledError:
            pass

    def login_flow(self, just_logged_in, app_refresh_interval=1800):
        if not app_refresh_interval or app_refresh_interval < 0:
            raise Exception('not app_refresh_interval or app_refresh_interval < 0')
        if app_refresh_interval > 21600:
            raise Exception('app_refresh_interval > 21600')

        if just_logged_in:
            self.logger.debug('Just logged in sequence start')
            self.zr_token = ''
            self.getAccountFamily()
            self.sendLauncherSync(False, False, True)
            self.fetchZeroRatingToken()
            self.syncUserFeatures()
            self.feed_timeline()
            self.reels_tray('cold_start')
            self.sendLauncherSync(False, False, True, True)
            self.reels_media([self.authenticated_user_id, ])
            self.news_inbox()
            self.getLoomFetchConfig()
            self.getDeviceCapabilitiesDecisions()
            self.getBootstrapUsers()
            self.user_info(self.authenticated_user_id)
            self.getLinkageStatus()
            self.sendSupportedCapabilities()
            self.getBlockedMedia()
            self.storeClientPushPermissions()
            self.getQPCooldowns()
            # $this->_registerPushChannels();
            self.reels_media([self.authenticated_user_id, ])
            self.explore_topical(None, None, True)
            self.getQPFetch()
            self.getProcessContactPointSignals()
            self.getArlinkDownloadInfo()
            # $this->_registerPushChannels();
            self.getSharePrefill()
            self.getPresences()
            self.direct_v2_inbox()
            self.direct_v2_inbox(None, 20, 10)
            # $this->_registerPushChannels();
            self.getFacebookOTA()
            self.logger.debug('Just logged in sequence end')

        else:
            session_expired = not self.last_login or time.time() - self.last_login > app_refresh_interval
            self.logger.debug('Session is ' + ('expired' if session_expired else 'fresh'))
            if session_expired:
                try:
                    self.reels_tray('cold_start')
                except ClientLoginRequiredError:
                    self.logger.debug('ClientLoginRequiredError in login flow, forcing login')
                    return self.login(force_login=True, app_refresh_interval=app_refresh_interval)
                self.feed_timeline(None, {'is_pull_to_refresh': None if session_expired else random.randint(1, 3) < 3,})
                self.getSharePrefill()
                self.news_inbox()
                self.getSharePrefill()
                self.news_inbox()
                self.user_info(self.authenticated_user_id)
                self.getDeviceCapabilitiesDecisions()
                self.getPresences()
                self.explore_topical()
                self.direct_v2_inbox()
                self.session_id = self.generate_uuid(False)
                self.tvguide()
                self.getLoomFetchConfig()
                self.getRankedRecipients('reshare', True)
                self.getRankedRecipients('raven', True)
                # $this->_registerPushChannels();
                self.logger.debug('End expired session flow')

            last_experiments = self.last_experiments
            if not last_experiments or time.time() - last_experiments > Constants.EXPERIMENTS_REFRESH:
                self.logger.debug('Experiments expired')
                self.syncUserFeatures()
                self.syncDeviceFeatures()
                self.logger.debug('Experiments expired flow end')

            if not self.zr_expires_at or time.time() - self.zr_expires_at > 0:
                self.logger.debug('Zero rating token expired')
                self.zr_token = None
                self.fetchZeroRatingToken('token_stale' if self.zr_expires_at and time.time() - self.zr_expires_at > 7200 else 'token_expired')
                self.logger.debug('Zero rating token expired flow end')

    def pre_login_flow(self):
        self.zr_token = ''
        self.fetchZeroRatingToken()
        self.bootstrapMsisdnHeader()
        self.readMsisdnHeader('ig_select_app')
        self.syncDeviceFeatures(prelogin=True)
        self.sendLauncherSync(prelogin=True)
        self.bootstrapMsisdnHeader()

        self.logAttribution()

        self.getPrefillCandidates()
        self.readMsisdnHeader('default', True)
        self.setContactPointPrefill('prefill')
        self.sendLauncherSync(True, True, True)
        self.syncDeviceFeatures(True, True)

    def login(self, force_login=False, app_refresh_interval=1800):
        """Login."""
        try:
            # Делаем паузы между вызовами, иначе инстаграм выкидывает ошибки что очень быстро делаются запросы
            self.call_api_throttle = 1
            if not self.is_maybe_logged_in or force_login:
                if force_login:
                    self.logger.debug('Forced login')

                self.pre_login_flow()

                if not self.csrftoken:
                    raise ClientError('Unable to get csrf from prelogin.')

                login_params = {
                    'country_codes': '[{"country_code":"1","source":["default"]}]',
                    'google_tokens': '[]',
                    'device_id': self.device_id,
                    'guid': self.uuid,
                    'adid': self.ad_id,
                    'phone_id': self.phone_id,
                    '_csrftoken': self.csrftoken,
                    'username': self.username,
                    'password': self.password,
                    'login_attempt_count': '0',
                }

                login_response = self._call_api(
                    'accounts/login/', params=login_params, return_response=True)

                if not self.csrftoken:
                    raise ClientError(
                        'Unable to get csrf from login.',
                        error_response=self._read_response(login_response))

                login_json = self._read_response(login_response)
                self.logger.debug('LOGIN RESPONSE: {0:d} {1!s}'.format(login_response.code, login_json))
                login_json = json.loads(login_json)

                if not login_json.get('logged_in_user', {}).get('pk'):
                    raise ClientLoginError('Unable to login.')

                self.is_maybe_logged_in = True
                self.last_login = time.time()
                self.login_flow(True, app_refresh_interval)

                if self.on_login:
                    on_login_callback = self.on_login
                    on_login_callback(self)
            else:
                # Эмуляция восстановления приложения
                self.logger.debug('Restoring session')
                self.login_flow(False, app_refresh_interval)
                self.last_login = time.time()
        finally:
            self.call_api_throttle = None

    def current_user(self):
        """Get current user info"""
        params = self.authenticated_params
        res = self._call_api('accounts/current_user/', params=params, query={'edit': 'true'})
        if self.auto_patch:
            ClientCompatPatch.user(res['user'], drop_incompat_keys=self.drop_incompat_keys)
        return res

    def edit_profile(self, first_name, biography, external_url, email, phone_number, gender):
        """
        Edit profile

        :param first_name:
        :param biography:
        :param external_url:
        :param email: Required.
        :param phone_number:
        :param gender: male: 1, female: 2, unspecified: 3
        :return:
        """
        if int(gender) not in [1, 2, 3]:
            raise ValueError('Invalid gender: {0:d}'.format(int(gender)))
        if not email:
            raise ValueError('Email is required.')

        params = {
            'username': self.authenticated_user_name,
            'gender': int(gender),
            'phone_number': phone_number or '',
            'first_name': first_name or '',
            'biography': biography or '',
            'external_url': external_url or '',
            'email': email,
        }
        params.update(self.authenticated_params)
        res = self._call_api('accounts/edit_profile/', params=params)
        if self.auto_patch:
            ClientCompatPatch.user(res.get('user'))
        return res

    def remove_profile_picture(self):
        """Remove profile picture"""
        res = self._call_api(
            'accounts/remove_profile_picture/', params=self.authenticated_params)
        if self.auto_patch:
            ClientCompatPatch.user(res['user'], drop_incompat_keys=self.drop_incompat_keys)
        return res

    def change_profile_picture(self, photo_data):
        """
        Change profile picture

        :param photo_data: byte string of image
        :return:
        """
        endpoint = 'accounts/change_profile_picture/'
        json_params = json.dumps(self.authenticated_params)
        hash_sig = self._generate_signature(json_params)
        fields = [
            ('ig_sig_key_version', self.key_version),
            ('signed_body', hash_sig + '.' + json_params)
        ]
        files = [
            ('profile_pic', 'profile_pic', 'application/octet-stream', photo_data)
        ]

        content_type, body = MultipartFormDataEncoder().encode(fields, files)

        headers = self.default_headers
        headers['Content-Type'] = content_type
        headers['Content-Length'] = len(body)

        endpoint_url = '{0}{1}'.format(self.api_url.format(version='v1'), endpoint)
        req = compat_urllib_request.Request(endpoint_url, body, headers=headers)
        try:
            self.logger.debug('POST {0!s}'.format(endpoint_url))
            response = self.opener.open(req, timeout=self.timeout)
        except compat_urllib_error.HTTPError as e:
            error_response = self._read_response(e)
            self.logger.debug('RESPONSE: {0:d} {1!s}'.format(e.code, error_response))
            self.process(e, error_response)
        except (SSLError, timeout, SocketError,
                compat_urllib_error.URLError,  # URLError is base of HTTPError
                compat_http_client.HTTPException) as connection_error:
            raise ClientConnectionError('{} {}'.format(
                connection_error.__class__.__name__, str(connection_error)))

        post_response = self._read_response(response)
        self.logger.debug('RESPONSE: {0:d} {1!s}'.format(response.code, post_response))
        json_response = json.loads(post_response)

        if self.auto_patch:
            ClientCompatPatch.user(json_response['user'], drop_incompat_keys=self.drop_incompat_keys)

        return json_response

    def set_account_private(self):
        """Make account private"""
        res = self._call_api('accounts/set_private/', params=self.authenticated_params)
        if self.auto_patch:
            ClientCompatPatch.list_user(res['user'], drop_incompat_keys=self.drop_incompat_keys)
        return res

    def set_account_public(self):
        """Make account public"""""
        res = self._call_api('accounts/set_public/', params=self.authenticated_params)
        if self.auto_patch:
            ClientCompatPatch.list_user(res['user'], drop_incompat_keys=self.drop_incompat_keys)
        return res

    def logout(self):
        """Logout user"""
        params = {
            'phone_id': self.phone_id,
            '_csrftoken': self.csrftoken,
            'guid': self.uuid,
            'device_id': self.device_id,
            '_uuid': self.uuid
        }
        return self._call_api('accounts/logout/', params=params, unsigned=True)

    def presence_status(self):
        """Get presence status setting"""
        json_params = json.dumps({}, separators=(',', ':'))
        query = {
            'ig_sig_key_version': self.key_version,
            'signed_body': self._generate_signature(json_params) + '.' + json_params
        }
        return self._call_api('accounts/get_presence_disabled/', query=query)

    def set_presence_status(self, disabled):
        """
        Set presence status setting

        :param disabled: True if disabling, else False
        """
        params = {
            'disabled': '1' if disabled else '0'
        }
        params.update(self.authenticated_params)
        return self._call_api('accounts/set_presence_disabled/', params=params)

    def enable_presence_status(self):
        """Enable presence status setting"""
        return self.set_presence_status(False)

    def disable_presence_status(self):
        """Disable presence status setting"""
        return self.set_presence_status(True)
