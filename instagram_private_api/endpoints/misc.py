import time
import warnings

from .common import ClientDeprecationWarning
from ..constants import Constants
from ..compatpatch import ClientCompatPatch


class MiscEndpointsMixin(object):
    """For miscellaneous functions."""

    def sync(self, prelogin=False):
        """Synchronise experiments."""
        if prelogin:
            params = {
                'id': self.generate_uuid(),
                'experiments': Constants.LOGIN_EXPERIMENTS
            }
        else:
            params = {
                'id': self.authenticated_user_id,
                'experiments': Constants.EXPERIMENTS
            }
            params.update(self.authenticated_params)
        return self._call_api('qe/sync/', params=params)

    def expose(self, experiment='ig_android_profile_contextual_feed'):  # pragma: no cover
        warnings.warn(
            'This endpoint is believed to be obsolete. Do not use.',
            ClientDeprecationWarning)

        params = {
            'id': self.authenticated_user_id,
            'experiment': experiment
        }
        params.update(self.authenticated_params)
        return self._call_api('qe/expose/', params=params)

    def megaphone_log(self, log_type='feed_aysf', action='seen', reason='', **kwargs):
        """
        A tracking endpoint of sorts

        :param log_type:
        :param action:
        :param reason:
        :param kwargs:
        :return:
        """
        params = {
            'type': log_type,
            'action': action,
            'reason': reason,
            '_uuid': self.uuid,
            'device_id': self.device_id,
            '_csrftoken': self.csrftoken,
            'uuid': self.generate_uuid(return_hex=True)
        }
        params.update(kwargs)
        return self._call_api('megaphone/log/', params=params, unsigned=True)

    def ranked_recipients(self):
        """Get ranked recipients"""
        res = self._call_api('direct_v2/ranked_recipients/', query={'show_threads': 'true'})
        return res

    def recent_recipients(self):
        """Get recent recipients"""
        res = self._call_api('direct_share/recent_recipients/')
        return res

    def news(self, **kwargs):
        """
        Get news feed of accounts the logged in account is following.
        This returns the items in the 'Following' tab.
        """
        return self._call_api('news/', query=kwargs)

    def news_inbox(self, prefetch=False):
        """
        Get inbox feed of activity related to the logged in account.
        This returns the items in the 'You' tab.
        """
        return self._call_api('news/inbox/', update_headers={'X-IG-Prefetch-Request': 'foreground'} if prefetch else {})

    def direct_v2_inbox(self, cursor_id=None, limit=0, thread_message_limit=None, prefetch=False):
        """Get v2 inbox"""
        headers = dict()
        query = dict(
            persistentBadging=True,
            visual_message_return_type='unseen',
            limit=limit,
        )
        if cursor_id:
            query['cursor'] = cursor_id
        if prefetch:
            headers['X-IG-Prefetch-Request'] = 'foreground'
        if thread_message_limit:
            query['thread_message_limit'] = thread_message_limit

        return self._call_api('direct_v2/inbox/', query=query, update_headers=headers)

    def oembed(self, url, **kwargs):
        """
        Get oembed info

        :param url:
        :param kwargs:
        :return:
        """
        query = {'url': url}
        query.update(kwargs)
        res = self._call_api('oembed/', query=query)
        return res

    def translate(self, object_id, object_type):
        """

        :param object_id: id value for the object
        :param object_type: One of [1, 2, 3] where
                            1 = CAPTION - unsupported
                            2 = COMMENT - unsupported
                            3 = BIOGRAPHY
        :return:
        """
        warnings.warn('This endpoint is not tested fully.', UserWarning)
        res = self._call_api(
            'language/translate/',
            query={'id': object_id, 'type': object_type})
        return res

    def bulk_translate(self, comment_ids):
        """
        Get translations of comments

        :param comment_ids: list of comment/caption IDs
        :return:
        """
        if isinstance(comment_ids, str):
            comment_ids = [comment_ids]
        query = {'comment_ids': ','.join(comment_ids)}
        res = self._call_api('language/bulk_translate/', query=query)
        return res

    def top_search(self, query):
        """
        Search for top matching hashtags, users, locations

        :param query: search terms
        :return:
        """
        res = self._call_api(
            'fbsearch/topsearch/',
            query={'context': 'blended', 'ranked_token': self.rank_token, 'query': query})
        if self.auto_patch and res.get('users', []):
            [ClientCompatPatch.list_user(u['user']) for u in res['users']]
        return res

    def stickers(self, sticker_type='static_stickers', location=None):
        """
        Get sticker assets

        :param sticker_type: One of ['static_stickers']
        :param location: dict containing 'lat', 'lng', 'horizontalAccuracy'.
                         Example: {'lat': '', 'lng': '', 'horizontalAccuracy': ''}
                         'horizontalAccuracy' is a float in meters representing the estimated horizontal accuracy
                         https://developer.android.com/reference/android/location/Location.html#getAccuracy()
        :return:
        """
        if sticker_type not in ['static_stickers']:
            raise ValueError('Invalid sticker_type: {0!s}'.format(sticker_type))
        if location and not ('lat' in location and 'lng' in location and 'horizontalAccuracy' in location):
            raise ValueError('Invalid location')
        params = {
            'type': sticker_type
        }
        if location:
            params['lat'] = location['lat']
            params['lng'] = location['lng']
            params['horizontalAccuracy'] = location['horizontalAccuracy']
        params.update(self.authenticated_params)
        return self._call_api('creatives/assets/', params=params)

    def setContactPointPrefill(self, usage):
        params = dict()
        params['_csrftoken'] = self.csrftoken
        params['phone_id'] = self.phone_id
        params['usage'] = usage

        return self._call_api('accounts/contact_point_prefill/', params=params)

    def logAttribution(self):
        params = dict()
        params['adid'] = self.ad_id
        return self._call_api('attribution/log_attribution/', params=params)

    def sendLauncherSync(self, prelogin,
                         idIsUuid=True,
                         useCsrfToken=False,
                         loginConfigs=False):

        params = dict()
        params[
            'configs'] = 'ig_android_felix_release_players,ig_user_mismatch_soft_error,ig_android_os_version_blocking_config,ig_android_carrier_signals_killswitch,fizz_ig_android,ig_mi_block_expired_events,ig_android_killswitch_perm_direct_ssim,ig_fbns_blocked'

        if idIsUuid:
            params['id'] = self.uuid
        else:
            params['id'] = self.authenticated_user_id

        if useCsrfToken:
            params['_csrftoken'] = self.csrftoken

        if loginConfigs:
            # LAUNCHER_LOGIN_CONFIGS
            params[
                'configs'] = 'ig_camera_ard_use_ig_downloader,ig_android_dogfooding,ig_android_bloks_data_release,ig_donation_sticker_public_thanks,ig_business_profile_donate_cta_android,ig_launcher_ig_android_network_dispatcher_priority_decider_qe2,ig_multi_decode_config,ig_android_improve_segmentation_hint,ig_android_memory_manager_holdout,ig_android_interactions_direct_sharing_comment_launcher,ig_launcher_ig_android_analytics_request_cap_qe,ig_direct_e2e_send_waterfall_sample_rate_config,ig_android_cdn_image_sizes_config,ig_android_critical_path_manager,ig_android_mobileboost_camera,ig_android_pdp_default_sections,ig_android_video_playback,ig_launcher_explore_sfplt_secondary_response_android,ig_android_upload_heap_on_oom,ig_synchronous_account_switch,ig_android_direct_presence_digest_improvements,ig_android_request_compression_launcher,ig_android_feed_attach_report_logs,ig_android_insights_welcome_dialog_tooltip,ig_android_qp_surveys_v1,ig_direct_requests_approval_config,ig_android_react_native_ota_kill_switch,ig_android_video_profiler_loom_traces,video_call_gk,ig_launcher_ig_android_network_stack_cap_video_request_qe,ig_shopping_android_business_new_tagging_flow,ig_android_igtv_bitrate,ig_android_geo_gating,ig_android_explore_startup_prefetch,ig_android_camera_asset_blocker_config,post_user_cache_user_based,ig_android_branded_content_story_partner_promote_rollout,ig_android_quic,ig_android_videolite_uploader,ig_direct_message_type_reporting_config,ig_camera_android_whitelist_all_effects_in_pre,ig_android_shopping_influencer_creator_nux,ig_android_mobileboost_blacklist,ig_android_direct_gifs_killswitch,ig_android_global_scheduler_direct,ig_android_image_display_logging,ig_android_global_scheduler_infra,ig_igtv_branded_content_killswitch,ig_cg_donor_duplicate_sticker,ig_launcher_explore_verified_badge_on_ads,ig_android_cold_start_class_preloading,ig_camera_android_attributed_effects_endpoint_api_query_config,ig_android_highlighted_products_business_option,ig_direct_join_chat_sticker,ig_android_direct_admin_tools_requests,ig_android_rage_shake_whitelist,ig_android_shopping_ads_cta_rollout,ig_android_igtv_segmentation,ig_launcher_force_switch_on_dialog,ig_android_iab_fullscreen_experience_config,ig_android_instacrash,ig_android_specific_story_url_handling_killswitch,ig_mobile_consent_settings_killswitch,ig_android_influencer_monetization_hub_launcher,ig_and roid_scroll_perf_mobile_boost_launcher,ig_android_cx_stories_about_you,ig_android_replay_safe,ig_android_stories_scroll_perf_misc_fixes_h2_2019,ig_android_shopping_django_product_search,ig_direct_giphy_gifs_rating,ig_android_ppr_url_logging_config,ig_canvas_ad_pixel,ig_strongly_referenced_mediacache,ig_android_direct_show_threads_status_in_direct,ig_camera_ard_brotli_model_compression,ig_image_pipeline_skip_disk_config,ig_android_explore_grid_viewpoint,ig_android_iab_persistent_process,ig_android_in_process_iab,ig_android_launcher_value_consistency_checker,ig_launcher_ig_explore_peek_and_sfplt_android,ig_android_skip_photo_finish,ig_biz_android_use_professional_account_term,ig_android_settings_search,ig_android_direct_presence_media_viewer,ig_launcher_explore_navigation_redesign_android,ig_launcher_ig_android_network_stack_cap_api_request_qe,ig_qe_value_consistency_checker,ig_stories_fundraiser_view_payment_address,ig_business_create_donation_android,ig_android_qp_waterfall_logging,ig_android_bloks_demos,ig_redex_dynamic_analysis,ig_android_bug_report_screen_record,ig_shopping_android_carousel_product_ids_fix_killswitch,ig_shopping_android_creators_new_tagging_flow,ig_android_direct_threads_app_dogfooding_flags,ig_shopping_camera_android,ig_android_qp_keep_promotion_during_cooldown,ig_android_qp_slot_cooldown_enabled_universe,ig_android_request_cap_tuning_with_bandwidth,ig_android_client_config_realtime_subscription,ig_launcher_ig_android_network_request_cap_tuning_qe,ig_android_concurrent_coldstart,ig_android_gps_improvements_launcher,ig_android_notification_setting_sync,ig_android_stories_canvas_mode_colour_wheel,ig_android_iab_session_logging_config,ig_android_network_trace_migration,ig_android_extra_native_debugging_info,ig_android_insights_top_account_dialog_tooltip,ig_launcher_ig_android_dispatcher_viewpoint_onscreen_updater_qe,ig_android_disable_browser_multiple_windows,ig_contact_invites_netego_killswitch,ig_android_update_items_header_height_launcher,ig_android_bulk_tag_untag_killswitch,ig_android_employee_options,ig_launcher_ig_android_video_pending_request_store_qe,ig_story_insights_entry,ig_android_creator_multi_select,ig_android_direct_new_media_viewer,ig_android_gps_profile_launcher,ig_android_direct_real_names_launcher,ig_fev_info_launcher,ig_android_remove_request_params_in_network_trace,ig_android_rageshake_redesign,ig_launcher_ig_android_network_stack_queue_undefined_request_qe,ig_cx_promotion_tooltip,ig_text_response_bottom_sheet,ig_android_carrier_signal_timestamp_max_age,ig_android_qp_xshare_to_fb,ig_android_rollout_gating_payment_settings,ig_android_mobile_boost_kill_switch,ig_android_betamap_cold_start,ig_android_media_store,ig_android_async_view_model_launcher,ig_android_newsfeed_recyclerview,ig_android_feed_optimistic_upload,ig_android_fix_render_backtrack_reporting,ig_delink_lasso_accounts,ig_android_feed_report_ranking_issue,ig_android_shopping_insights_events_validator,ig_biz_android_new_logging_architecture,ig_launcher_ig_android_reactnative_realtime_ota,ig_android_boomerang_crash_android_go,ig_android_shopping_influencer_product_sticker_editing,ig_camera_android_max_vertex_texture_launcher,bloks_suggested_hashtag'
        else:
            # LAUNCHER_CONFIGS
            params[
                'configs'] = 'ig_android_media_codec_info_collection,stories_gif_sticker,ig_android_felix_release_players,bloks_binding,ig_android_camera_network_activity_logger,ig_android_os_version_blocking_config,ig_android_carrier_signals_killswitch,live_special_codec_size_list,fbns,ig_android_aed,ig_client_config_server_side_retrieval,ig_android_bloks_perf_logging,ig_user_session_operation,ig_user_mismatch_soft_error,ig_android_prerelease_event_counter,fizz_ig_android,ig_android_vc_clear_task_flag_killswitch,ig_android_killswitch_perm_direct_ssim,ig_android_codec_high_profile,ig_android_smart_prefill_killswitch,sonar_prober,action_bar_layout_width,ig_auth_headers_device,always_use_server_recents'

        if prelogin:
            pass
        else:
            params['_uuid'] = self.uuid
            params['_uid'] = self.uuid

        return self._call_api('launcher/sync/', params=params)

    def syncDeviceFeatures(self, prelogin=False, useCsrfToken=False):
        params = dict(
            id=self.uuid,
            experiments=Constants.LOGIN_EXPERIMENTS
        )

        if useCsrfToken:
            params['_csrftoken'] = self.csrftoken

        if not prelogin:
            params['_uuid'] = self.uuid
            params['_uid'] = self.authenticated_user_id
        return self._call_api('qe/sync/', params=params, update_headers={'X-DEVICE-ID': self.uuid})

    def bootstrapMsisdnHeader(self, usage='ig_select_app'):
        params = dict(
            device_id=self.uuid,
            mobile_subno_usage=usage
        )
        return self._call_api('accounts/msisdn_header_bootstrap/', params=params)

    def readMsisdnHeader(self, usage, useCsrfToken=False):
        params = dict(
            device_id=self.uuid,
            mobile_subno_usage=usage
        )
        if useCsrfToken:
            params['_csrftoken'] = self.csrftoken
        return self._call_api('accounts/read_msisdn_header/', params=params, update_headers={'X-DEVICE-ID': self.uuid})

    def fetchZeroRatingToken(self, reason='token_expired'):
        zr_params = {
            'device_id': self.device_id,
            'custom_device_id': self.uuid,
            'fetch_reason': reason,
            'token_hash': self.zr_token,
        }
        zr_json = self._call_api('zr/token/result/', query=zr_params)
        self.zr_expires_at = time.time() + zr_json['token']['ttl']
        self.zr_token = zr_json['token']['token_hash']

    def getPrefillCandidates(self):
        params = dict(
            android_device_id=self.device_id,
            device_id=self.uuid,
            usages='["account_recovery_omnibox"]'
        )
        return self._call_api('accounts/get_prefill_candidates/', params=params)
